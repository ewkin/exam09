import React from 'react';
import './NavList.css';
import NavItem from './NavItem/NavItem';

const NavList = () => {
    return (
        <ul className="NavigationItems">
            <NavItem to="/" exact>Home</NavItem>
            <NavItem to="/new-contact" exact>Add new contact</NavItem>
        </ul>
    );
};

export default NavList;