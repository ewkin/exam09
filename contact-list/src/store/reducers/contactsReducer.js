import {
    FETCH_CONTACT_FAILURE,
    FETCH_CONTACT_REQUEST,
    FETCH_CONTACT_SUCCESS,
    INIT_CONTACT, SHOW_CONTACT, UPDATE_INIT_CONTACT
} from "../actions/contactsAction";

const initialState = {
    fetchLoading: false,
    fetchError: false,
    updateState:true,
    showContact:false,
    contact:{},
    contacts: []
};

export const contactsReducer = (state = initialState, action) => {
    switch (action.type) {
        case INIT_CONTACT:
            return {...state, contacts: action.contacts};
        case UPDATE_INIT_CONTACT:
            return {...state, updateState: action.value};
        case FETCH_CONTACT_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_CONTACT_SUCCESS:
            return {...state, fetchLoading: false};
        case FETCH_CONTACT_FAILURE:
            return {...state, fetchLoading: false, fetchError: action.error};
        case SHOW_CONTACT:
            return {...state, showContact: action.value, contact: action.contact};
        default:
            return state;
    }
};