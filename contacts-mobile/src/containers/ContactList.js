import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContacts, showContact} from "../store/actions/contactAction";
import {
    FlatList,
    Image,
    SafeAreaView,
    StyleSheet,
    Text,
    TouchableOpacity,
    Alert,
    Linking,
    Modal,
    View
} from "react-native";
import {StatusBar} from "expo-status-bar";


export default function ContactList() {
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts);
    const contact = useSelector(state => state.contact);
    const shouldShow = useSelector(state => state.showContact);


    useEffect(() => {
        dispatch(fetchContacts())
    }, [dispatch,]);

    const contactCancelHandler = () => {
        dispatch(showContact(false, {}));
    };

    const contactShowHandler = contact => {
        dispatch(showContact(true, contact));
    };


    const dialCall = () => {
        let phoneNumber = '';

        if (Platform.OS === 'android') {
            phoneNumber = 'tel:${' + contact.number + '}';
        } else {
            phoneNumber = 'telprompt:${' + contact.number + '}';
        }
        Linking.openURL(phoneNumber);
    };

    const renderItem = ({item}) => (
        <TouchableOpacity onPress={() => contactShowHandler(item)} style={styles.item}>
            <Image
                style={styles.avatar}
                source={{uri: item.url}}
            />
            <Text style={styles.title}>{item.name}</Text>
        </TouchableOpacity>
    );
    return (
        <>
            <Modal
                animationType="slide"
                transparent={true}
                visible={shouldShow}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    contactCancelHandler();
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Image
                            style={styles.avatar}
                            source={{uri: contact.url}}
                        />
                        <Text style={styles.title}>{contact.name}</Text>
                        <TouchableOpacity onPress={() => Linking.openURL('mailto:'+contact.email)}>
                            <Text style={styles.modalText}>{contact.email}</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={dialCall}>
                            <Text style={styles.modalText}>{contact.phone}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={[styles.button, styles.buttonClose]}
                            onPress={() => contactCancelHandler()}
                        >
                            <Text style={styles.textStyle}>Hide Modal</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            <SafeAreaView style={styles.container}>
                <FlatList
                    data={contacts}
                    renderItem={renderItem}
                    keyExtractor={contact => contact.id}
                />
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: '#3d72a4',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 20,
        color: '#ffffff',
    },
    avatar: {
        width: 66,
        height: 66,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonClose: {
        backgroundColor: "#2196F3",
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textDecorationLine: 'underline',
        textAlign: "center"
    }
});

