import React from "react";
import {StyleSheet, View} from "react-native";
import {NativeRouter} from "react-router-native";
import ContactList from "./src/containers/ContactList";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {contactsReducer} from "./src/store/reducers/contactReducer";
import {Provider} from "react-redux";


const enhancers = applyMiddleware(thunk);
const store = createStore(contactsReducer, enhancers);


export default function App (){
    return (
        <Provider store={store}>
            <NativeRouter>
                <View style={styles.container}>
                    <View style={styles.nav}>
                        <ContactList/>
                    </View>
                </View>
            </NativeRouter>
        </Provider>

    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        padding: 10
    },

    nav: {
        flexDirection: "row",
        justifyContent: "space-around"
    },
    navItem: {
        flex: 1,
        alignItems: "center",
        padding: 10
    },

});

